package cat.itb.cityquiz.ui.main.screens.quiz;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    MutableLiveData<Game> game = new MutableLiveData<>();

    public void startQuiz(){
        Game newGame = gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);
        game.postValue(newGame);
//        gameLogic.answerQuestions(game,3);
    }

    public MutableLiveData<Game> getGame() {
        return game;
    }

    public void answerQuestions(int number){
        Game newGame = gameLogic.answerQuestions(game.getValue(),number);
        game.postValue(newGame);
    }

}
