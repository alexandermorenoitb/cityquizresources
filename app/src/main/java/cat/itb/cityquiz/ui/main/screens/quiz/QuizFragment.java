package cat.itb.cityquiz.ui.main.screens.quiz;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.ui.main.screens.start.StartFragment;

public class QuizFragment extends Fragment {

    private QuizViewModel quizViewModel;
    TextView option1;
    TextView option2;
    TextView option3;
    TextView option4;
    TextView option5;
    TextView option6;


    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.quiz_fragment_row, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        quizViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onGameChanged(Game game) {
        if(game.isFinished()){
            Navigation.findNavController(getView()).navigate(R.id.action_quizFragment_to_resultFragment);
        } else
            display(game);
    }


    private void display(Game game) {

        String foto = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
//        game.getCurrentQuestion().getCorrectCity();
        int resId = getContext().getResources().getIdentifier(foto,"drawable",getContext().getPackageName());
        ImageView imageView = getView().findViewById(R.id.cityImageView);
        imageView.setImageResource(resId);


        //        Question question = game.getCurrentQuestion();
//        question.getCorrectCity();
//        question.getPossibleCities();

        option1 = getView().findViewById(R.id.btn1country);
        option2 = getView().findViewById(R.id.btn2country);
        option3 = getView().findViewById(R.id.btn3country);
        option4 = getView().findViewById(R.id.btn4country);
        option5 = getView().findViewById(R.id.btn5country);
        option6 = getView().findViewById(R.id.btn6country);

        List<City> possibleAnswers = game.getCurrentQuestion().getPossibleCities();

        option1.setText(possibleAnswers.get(0).getName());
        option2.setText(possibleAnswers.get(1).getName());
        option3.setText(possibleAnswers.get(2).getName());
        option4.setText(possibleAnswers.get(3).getName());
        option5.setText(possibleAnswers.get(4).getName());
        option6.setText(possibleAnswers.get(5).getName());

        option1.setOnClickListener(this::onViewClicked);
        option2.setOnClickListener(this::onViewClicked);
        option3.setOnClickListener(this::onViewClicked);
        option4.setOnClickListener(this::onViewClicked);
        option5.setOnClickListener(this::onViewClicked);
        option6.setOnClickListener(this::onViewClicked);
    }

    public void questionAnswered(int number){
        quizViewModel.answerQuestions(number);

    }

    public void onViewClicked(View view){

        int response = 0;

        switch (view.getId()){
            case R.id.btn1country:
                response = 0;
                questionAnswered(response);
                break;

            case R.id.btn2country:
                response = 1;
                questionAnswered(response);
                break;

            case R.id.btn3country:
                response = 2;
                questionAnswered(response);
                break;

            case R.id.btn4country:
                response = 3;
                questionAnswered(response);
                break;

            case R.id.btn5country:
                response = 4;
                questionAnswered(response);
                break;

            case R.id.btn6country:
                response = 5;
                questionAnswered(response);
                break;
        }

    }

}
