package cat.itb.cityquiz.ui.main.screens.result;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.ui.main.screens.quiz.QuizViewModel;

public class ResultFragment extends Fragment {

    private QuizViewModel quizViewModel;
    private Game game;
    private TextView textView;

    public static ResultFragment newInstance() {
        return new ResultFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.result_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        //game = quizViewModel.getGame();
        quizViewModel.getGame().observe(this, this::onGameChanged);
        getView().findViewById(R.id.btnPlayAgain).setOnClickListener(this::changeToStartFragment);

    }

    private void onGameChanged(Game game) {

        if(game.isFinished()){
            textView = getView().findViewById(R.id.numberScore);
            textView.setText(game.getScore()+"");
        } else{
            Navigation.findNavController(getView()).navigate(R.id.action_resultFragment_to_quizFragment);
        }


    }

//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        view.findViewById(R.id.btnPlayAgain).setOnClickListener(this::changeToStartFragment);
//    }

    private void changeToStartFragment(View view){
        Navigation.findNavController(view).navigate(R.id.action_resultFragment_to_quizFragment);
    }

}
