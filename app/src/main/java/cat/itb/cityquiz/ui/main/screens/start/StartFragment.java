package cat.itb.cityquiz.ui.main.screens.start;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.ui.main.screens.quiz.QuizViewModel;

public class StartFragment extends Fragment {


    private QuizViewModel quizViewModel;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        view.findViewById(R.id.btnStart).setOnClickListener(this::changeToQuizFragment);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        quizViewModel.getGame().observe(this, this::onGameChanged);


    }

    private void onGameChanged(Game game) {
        if (game != null)
            Navigation.findNavController(getView()).navigate(R.id.action_startFragment_to_quizFragment);
    }

    private void changeToQuizFragment(View view) {
        quizViewModel.startQuiz();
    }
}
